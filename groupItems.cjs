const items = require('./3-arrays-vitamins.cjs');

const fetchVitamins = () => {

    let vitaminString = "";

    items.forEach(element => {
        let vitamin = element.contains;
        vitaminString = vitaminString + vitamin + ", ";

    });
    vitaminString = vitaminString.substring(0, vitaminString.length - 2)

    let groupVitamins = vitaminString.split(', ')

    // console.log(groupVitamins)

    return groupVitamins;
}

const nonDuplicate = (groupVitamins) => {

    let nonDuplicateVitamins = [];
    nonDuplicateVitamins.push(groupVitamins[0])

    groupVitamins.forEach(element1 => {

        let flag = 0;
        nonDuplicateVitamins.forEach(element2 => {
            if (element1.includes(element2))
                flag++;
        });
        if (flag === 0)
            nonDuplicateVitamins.push(element1)
    });
    // console.log(nonDuplicateVitamins)
    return nonDuplicateVitamins;

}

const group = (nonDuplicateVitamins) => {
    //console.log(nonDuplicateVitamins)
    nonDuplicateVitamins.forEach(element => {

        console.log(`The items having the ${element} are \n`)

        items.forEach(key => {

            if (key.contains.includes(element)){
                console.log(key)
            }
        });
    });
}
const groupVitamins = fetchVitamins();

const nonDuplicateVitamins = nonDuplicate(groupVitamins);
group(nonDuplicateVitamins)
