const items = require('./3-arrays-vitamins.cjs')

const availableItems = () => {

    let result = [];
    items.forEach((element) => {
        
        if (element.available === true)
            result.push(element)
    })

    return result
}

console.log(availableItems())