const items = require('./3-arrays-vitamins.cjs')

const numberOfVitamins = () => {
    const result = items.sort((a, b) => {

        const value1 = a.contains.split(',').length;

        const value2 = b.contains.split(',').length;

        return value2 - value1;
    })
    return result;
}

console.log(numberOfVitamins())